package de.awacademy.javapdf;

import com.lowagie.text.Document;
import com.lowagie.text.Element;
import com.lowagie.text.Font;
import com.lowagie.text.FontFactory;
import com.lowagie.text.Image;
import com.lowagie.text.Jpeg;
import com.lowagie.text.PageSize;
import com.lowagie.text.Paragraph;
import com.lowagie.text.Phrase;
import com.lowagie.text.Rectangle;
import com.lowagie.text.RomanList;
import com.lowagie.text.pdf.CMYKColor;
import com.lowagie.text.pdf.PdfAction;
import com.lowagie.text.pdf.PdfAnnotation;
import com.lowagie.text.pdf.PdfContentByte;
import com.lowagie.text.pdf.PdfFormField;
import com.lowagie.text.pdf.PdfGState;
import com.lowagie.text.pdf.PdfName;
import com.lowagie.text.pdf.PdfPCell;
import com.lowagie.text.pdf.PdfPTable;
import com.lowagie.text.pdf.PdfWriter;
import com.lowagie.text.pdf.RadioCheckField;
import com.lowagie.text.pdf.TextField;

import java.awt.Color;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class A02_StyledPDFDocument_Aufgabe2 {

    private static final boolean ENCRYPT_DOCUMENT = false;

    private static final String USER_PASSWORD = "geheim";


    private Document document;

    private PdfWriter pdfWriter;

    public static void main(String[] args) throws IOException {
        new A02_StyledPDFDocument_Aufgabe2().run();
    }

    private void run() throws IOException {
        File pdfFile = new File("styledDocument.pdf");
        this.document = new Document(PageSize.A4);
        this.pdfWriter = PdfWriter.getInstance(document, new FileOutputStream(pdfFile));

        if (ENCRYPT_DOCUMENT) {
            pdfWriter.setEncryption(USER_PASSWORD.getBytes(), "owner".getBytes(), PdfWriter.ALLOW_PRINTING, PdfWriter.ENCRYPTION_AES_128);
        }

        document.open();

        printFonts();

        printEmptyLine();
        printColors();

        printEmptyLine();
        printImage();

        printEmptyLine();
        printTable();

        printEmptyLine();
        printAlignment();

        printEmptyLine();
        printBlocksatz();

        document.newPage();
        printList("Auflistung mit Strichen", false, false);
        printEmptyLine();
        printList("Auflistung mit Zahlen", true, false);
        printEmptyLine();
        printList("Auflistung mit Buchstaben", false, true);
        printEmptyLine();
        printRomanList("Römische Nummerierung Großbuchstaben", false);
        printEmptyLine();
        printRomanList("Römische Nummerierung Kleinbuchstaben", true);
        printEmptyLine();
        printListWithSubList();

        document.newPage();
        printWatermarkImage();

        printForm();

        printEmptyLine();
        printLink();

        document.close();
    }

    private void printEmptyLine() {
        document.add(new Paragraph(" "));
    }

    private void printFonts() {
        List<String> fontNames = new ArrayList<>();
        fontNames.addAll(FontFactory.getRegisteredFonts());
        Collections.sort(fontNames);

        for (String fontName : fontNames) {
            Font font = FontFactory.getFont(fontName);
            Paragraph paragraph = new Paragraph(fontName);
            paragraph.setFont(font);
            paragraph.add(": Herzlich Willkommen zum Workshop Java - PDF!");
            document.add(paragraph);
        }
    }

    private void printColors() {
        Color[] colors = {Color.RED, Color.GREEN, Color.BLUE, new CMYKColor(0, 255, 0, 0)};
        Font font = new Font(Font.HELVETICA, 12, Font.BOLD);
        for (Color color : colors) {
            Paragraph paragraph = new Paragraph();
            font.setColor(color);
            paragraph.setFont(font);
            paragraph.add("Farbe");
            document.add(paragraph);
        }
    }

    private void printImage() throws IOException {
        Jpeg jpg = new Jpeg(new File("resources/aw_academy_logo.jpg").toURI().toURL());
        jpg.setAlignment(Element.ALIGN_CENTER);
        jpg.scalePercent(50);
        document.add(jpg);
    }

    private void printTable() {
        PdfPTable table = new PdfPTable(2);
        table.setHeaderRows(1);
        table.setWidthPercentage(80);
        table.setWidths(new float[] {3.0f, 1.0f});

        PdfPCell column1 = new PdfPCell(new Phrase("Veranstaltung"));
        column1.setHorizontalAlignment(Element.ALIGN_CENTER);
        table.addCell(column1);

        PdfPCell column2 = new PdfPCell(new Phrase("Datum"));
        column2.setHorizontalAlignment(Element.ALIGN_CENTER);
        table.addCell(column2);

        table.addCell("Workshop Java - Excel");
        table.addCell("08.07.2020");
        table.addCell("Workshop Java - PDF");
        table.addCell("14.07.2020");
        table.addCell("Workshop Java - XML");
        table.addCell("23.07.2020");

        document.add(table);
    }

    private void printAlignment() {
        Paragraph paragraphLeft = new Paragraph("Dieser Text erscheint linksbündig.");
        paragraphLeft.setAlignment(Element.ALIGN_LEFT);
        document.add(paragraphLeft);

        Paragraph paragraphCenter = new Paragraph("Diser Text erscheint zentriert.");
        paragraphCenter.setAlignment(Element.ALIGN_CENTER);
        document.add(paragraphCenter);

        Paragraph paragraphRight = new Paragraph("Dieser Text erscheint rechtsbündig.");
        paragraphRight.setAlignment(Element.ALIGN_RIGHT);
        document.add(paragraphRight);

        Paragraph paragraphIdentation = new Paragraph("Dieser Text erscheint linksbündig, aber eingerückt.");
        paragraphIdentation.setAlignment(Element.ALIGN_LEFT);
        paragraphIdentation.setIndentationLeft(30);
        document.add(paragraphIdentation);
    }

    private void printBlocksatz() {
        Paragraph paragraphBlocksatz = new Paragraph();
        paragraphBlocksatz.setIndentationLeft(150);
        paragraphBlocksatz.setIndentationRight(150);
        paragraphBlocksatz.setAlignment(Element.ALIGN_JUSTIFIED);
        paragraphBlocksatz.add("Es war einmal vor langer langer Zeit ein kleiner Paragraph, der sich einfach nicht sauber im Blocksatz formatieren lassen wollte.");
        document.add(paragraphBlocksatz);

    }

    private void printList(String description, boolean numbered, boolean lettered) {
        Paragraph paragraph = new Paragraph(description);
        document.add(paragraph);

        com.lowagie.text.List list = new com.lowagie.text.List(numbered, lettered);
        list.add("Anton");
        list.add("Berta");
        list.add("Cesar");
        document.add(list);
    }

    private void printRomanList(String description, boolean lowercase) {
        Paragraph paragraph = new Paragraph(description);
        document.add(paragraph);

        RomanList romanList = new RomanList(lowercase, 15);
        romanList.add("Anton");
        romanList.add("Berta");
        romanList.add("Cesar");
        document.add(romanList);
    }

    private void printListWithSubList() {
        Paragraph paragraph = new Paragraph("Geschachtelte Listen:");
        document.add(paragraph);

        com.lowagie.text.List list = new com.lowagie.text.List(false, true, 15);

        list.add("Desktop");
        com.lowagie.text.List desktopList = new com.lowagie.text.List(true, false, 15);
        desktopList.add("Windows");
        desktopList.add("MacOS");
        desktopList.add("Linux");
        list.add(desktopList);

        list.add("Mobil");
        com.lowagie.text.List mobilList = new com.lowagie.text.List(true, false, 15);
        mobilList.add("Android");
        mobilList.add("iOS");
        mobilList.add("Windows Phone");
        list.add(mobilList);

        document.add(list);
    }

    private void printWatermarkImage() throws IOException {
        Image image = Image.getInstance("resources/aw_academy_logo.png");
        image.setAbsolutePosition(200, 700);
        //image.scaleAbsolute(100, 100);

        PdfContentByte pdfContentByte = pdfWriter.getDirectContentUnder();
        pdfContentByte.saveState();

        PdfGState state = new PdfGState();
        state.setFillOpacity(0.2f);

        pdfContentByte.setGState(state);
        pdfContentByte.addImage(image);
        pdfContentByte.restoreState();
    }

    private void printForm() throws IOException {
        Paragraph firstNameParagraph = new Paragraph("Vorname:");
        document.add(firstNameParagraph);
        printEmptyLine();

        TextField firstNameTextField = new TextField(pdfWriter, new Rectangle(150, 782, 330, 806), "firstName");
        firstNameTextField.setBorderColor(Color.BLUE);
        firstNameTextField.setBorderWidth(1);
        firstNameTextField.setTextColor(Color.BLACK);
        firstNameTextField.setFontSize(12);
        firstNameTextField.setText("");
        PdfFormField firstNamePdfField = firstNameTextField.getTextField();
        pdfWriter.addAnnotation(firstNamePdfField);

        Paragraph lastNameParagraph = new Paragraph("Nachname:");
        document.add(lastNameParagraph);
        printEmptyLine();

        TextField lastNameTextField = new TextField(pdfWriter, new Rectangle(150, 746, 330, 770), "lastName");
        lastNameTextField.setBorderColor(Color.BLUE);
        lastNameTextField.setBorderWidth(1);
        lastNameTextField.setTextColor(Color.BLACK);
        lastNameTextField.setFontSize(12);
        lastNameTextField.setText("");
        PdfFormField lastNamePdfField = lastNameTextField.getTextField();
        pdfWriter.addAnnotation(lastNamePdfField);

        Paragraph consultantParagraph = new Paragraph("Bin ein Consultant:");
        document.add(consultantParagraph);
        printEmptyLine();

        RadioCheckField consultantCheckField = new RadioCheckField(pdfWriter, new Rectangle(150, 710, 170, 734), "consultant", "check");
        consultantCheckField.setBorderColor(Color.BLUE);
        consultantCheckField.setBorderWidth(1);
        consultantCheckField.setTextColor(Color.BLACK);
        consultantCheckField.setFontSize(12);
        consultantCheckField.setChecked(false);
        PdfFormField consultantPdfField = consultantCheckField.getCheckField();
        pdfWriter.addAnnotation(consultantPdfField);
    }

    public void printLink() throws MalformedURLException {
        PdfAction linkAction = new PdfAction(new URL("http://www.awacademy.de/"));
        PdfAnnotation linkAnnotation = PdfAnnotation.createLink(pdfWriter, new Rectangle(85f, 675f, 155f, 657f), PdfName.LINK, linkAction);
        linkAnnotation.setColor(Color.WHITE);

        Paragraph paragraph = new Paragraph("see also: ");
        Font font = new Font(Font.HELVETICA, 12, Font.UNDERLINE);
        paragraph.setFont(font);
        paragraph.add("Homepage");
        document.add(paragraph);
        pdfWriter.addAnnotation(linkAnnotation);
    }

    // Aufgabe 2: Erzeuge ein beliebiges PDF-Dokument, z. B.: Rechnung, Bestellformular, Kfz-Anmeldeformular oder was immer Dir einfällt.
    //            Recherchiere im Internet, falls Du Elemente verwenden möchtest, die hier nicht behandelt wurden.
    //            Tipp: Suche ggf. auch nach iText, um Dokumentation im Internet zu finden.
    //                  Aber Achtung: Manche Quellen beziehen sich auf iText 5 oder neuer und sind damit nicht immer OpenPDF-kompatibel.
}
