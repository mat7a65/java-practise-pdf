public class Rechnung {

    private Bestellung bestellung;
    private String rechnungsDatum;
    private int rechnungsNummer;

    public Rechnung(Bestellung bestellung, String rechnungsDatum, int rechnungsNummer) {
        this.bestellung = bestellung;
        this.rechnungsDatum = rechnungsDatum;
        this.rechnungsNummer = rechnungsNummer;
    }

    public Bestellung getBestellung() {
        return bestellung;
    }

    public void setBestellung(Bestellung bestellung) {
        this.bestellung = bestellung;
    }

    public String getRechnungsDatum() {
        return rechnungsDatum;
    }

    public void setRechnungsDatum(String rechnungsDatum) {
        this.rechnungsDatum = rechnungsDatum;
    }

    public int getRechnungsNummer() {
        return rechnungsNummer;
    }

    public void setRechnungsNummer(int rechnungsNummer) {
        this.rechnungsNummer = rechnungsNummer;
    }
}
