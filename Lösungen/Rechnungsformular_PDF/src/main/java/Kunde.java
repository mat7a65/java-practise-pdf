public class Kunde {

    private int kundenNummer;
    private String anrede;
    private String name;
    private String strasse;
    private String plz;
    private String stadt;

    public Kunde(int kundenNummer, String anrede, String name, String strasse, String plz, String stadt) {
        this.kundenNummer = kundenNummer;
        this.anrede = anrede;
        this.name = name;
        this.strasse = strasse;
        this.plz = plz;
        this.stadt = stadt;
    }

    public int getKundenNummer() {
        return kundenNummer;
    }

    public void setKundenNummer(int kundenNummer) {
        this.kundenNummer = kundenNummer;
    }

    public String getAnrede() {
        return anrede;
    }

    public void setAnrede(String anrede) {
        this.anrede = anrede;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getStrasse() {
        return strasse;
    }

    public void setStrasse(String strasse) {
        this.strasse = strasse;
    }

    public String getPlz() {
        return plz;
    }

    public void setPlz(String plz) {
        this.plz = plz;
    }

    public String getStadt() {
        return stadt;
    }

    public void setStadt(String stadt) {
        this.stadt = stadt;
    }
}
