public class Artikel {

    private int artikelNummer;
    private String artikelBereichnung;
    private double preis;
    private int mehrwertSteuer;

    public Artikel(int artikelNummer, String artikelBereichnung, double preis, int mehrwertSteuer) {
        this.artikelNummer = artikelNummer;
        this.artikelBereichnung = artikelBereichnung;
        this.preis = preis;
        this.mehrwertSteuer = mehrwertSteuer;
    }


    public int getArtikelNummer() {
        return artikelNummer;
    }

    public void setArtikelNummer(int artikelNummer) {
        this.artikelNummer = artikelNummer;
    }

    public String getArtikelBezeichnung() {
        return artikelBereichnung;
    }

    public void setArtikelBereichnung(String artikelBereichnung) {
        this.artikelBereichnung = artikelBereichnung;
    }

    public double getPreis() {
        return preis;
    }

    public void setPreis(double preis) {
        this.preis = preis;
    }

    public int getMehrwertSteuer() {
        return mehrwertSteuer;
    }

    public void setMehrwertSteuer(int mehrwertSteuer) {
        this.mehrwertSteuer = mehrwertSteuer;
    }
}
