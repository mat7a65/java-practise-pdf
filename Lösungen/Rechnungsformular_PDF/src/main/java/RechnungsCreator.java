import com.lowagie.text.*;
import com.lowagie.text.pdf.*;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

public class RechnungsCreator {

    private static final boolean ENCRYPT_DOCUMENT = false;

    private static final String USER_PASSWORD = "secret";
    private Document document;
    private PdfWriter pdfWriter;

    public static void main(String[] args) throws IOException {
        new RechnungsCreator().run();
    }

    private void run() throws IOException {
        File pdfFile = new File("rechnung_example.pdf");
        this.document = new Document(PageSize.A4);
        this.pdfWriter = PdfWriter.getInstance(document, new FileOutputStream(pdfFile));
        if (ENCRYPT_DOCUMENT) {
            pdfWriter.setEncryption(USER_PASSWORD.getBytes(), "owner".getBytes(),
                    PdfWriter.ALLOW_PRINTING, PdfWriter.ENCRYPTION_AES_128);
        }


        document.open();

        printHeader();

        printEmptyLine();
        printEmptyLine();

        printInfos();

        printAbsender();

        printEmptyLine();
        printEmptyLine();

        printAnschift();

        printEmptyLine();
        printEmptyLine();
        printEmptyLine();
        printEmptyLine();
        printEmptyLine();

        printRechnung();

        printEmptyLine();

        printText();

        document.close();
    }

    //    Erstelle Dummy-Data
    Kunde kunde = new Kunde(
            1,
            "Herr",
            "Donald Duck",
            "Blumenstr. 13",
            "406080",
            "Entenhausen"
    );

    Artikel artikelKopfhörer = new Artikel(
            1,
            "Kopfhörer",
            49.95f,
            19
    );

    Artikel artikelLustigesTaschenbuch = new Artikel(
            2,
            "Lustiges Taschenbuch",
            7.85f,
            19
    );

    Artikel artikelChips = new Artikel(
            3,
            "Chips",
            0.95f,
            19
    );

    Bestellung bestellung1 = new Bestellung(
            1,
            "10.07.2020",
            "25.07.2020",
            kunde,
            artikelChips
    );

    Rechnung rechnung = new Rechnung(
            bestellung1,
            "22.07.2020",
            1
    );

    Absender absender = new Absender(
            "Concept Solution",
            "Alte Strasse 3",
            "406080",
            "Entenhausen"
    );


    //    Leerzeile
    private void printEmptyLine() {
        document.add(new Paragraph(" "));
    }


    //    Kopfzeile
    private void printHeader() {
        Paragraph paragraphLeft = new Paragraph(absender.getUnternehmen());
        paragraphLeft.setAlignment(Element.ALIGN_CENTER);
        paragraphLeft.setIndentationRight(30);
        document.add(paragraphLeft);
    }

    //    Randinformationen
    private void printInfos() {
        Paragraph paragraph = new Paragraph();
        paragraph.setAlignment(Element.ALIGN_LEFT);
        paragraph.setIndentationLeft(375);

        paragraph.add(new Paragraph());
        paragraph.add("Rech-Nr.: ");
        paragraph.add(String.valueOf(rechnung.getRechnungsNummer()));
        paragraph.add(new Paragraph());
        paragraph.add("Kd-Nr-.:  ");
        paragraph.add(String.valueOf(rechnung.getBestellung().getKunde().getKundenNummer()));
        paragraph.add(new Paragraph());
        paragraph.add("Rech-Dat.: " + rechnung.getRechnungsDatum());
        paragraph.add(new Paragraph());
        paragraph.add("Liefer-Dat.: " + rechnung.getBestellung().getLieferDatum());


        document.add(paragraph);
    }

    private void printAbsender() {
        Paragraph paragraph = new Paragraph();
        paragraph.setAlignment(Element.ALIGN_LEFT);
        paragraph.setIndentationLeft(30);

        paragraph.add(absender.getUnternehmen());
        paragraph.add(new Paragraph());
        paragraph.add(absender.getStrasse());
        paragraph.add(new Paragraph());
        paragraph.add(absender.getPlz() + " " + absender.getStadt());

        document.add(paragraph);
    }


    //    Anschrift

    private void printAnschift() {
        Paragraph paragraph = new Paragraph();
        paragraph.setAlignment(Element.ALIGN_LEFT);
        paragraph.setIndentationLeft(30);

        paragraph.add(rechnung.getBestellung().getKunde().getAnrede());
        paragraph.add(new Paragraph());
        paragraph.add(rechnung.getBestellung().getKunde().getName());
        paragraph.add(new Paragraph());
        paragraph.add(rechnung.getBestellung().getKunde().getStrasse());
        paragraph.add(new Paragraph());
        paragraph.add(rechnung.getBestellung().getKunde().getPlz() + " " + rechnung.getBestellung().getKunde().getStadt());
        document.add(paragraph);
    }


    private void printRechnung() {

        Paragraph paragraph = new Paragraph();
        paragraph.add("Rechnung");
        paragraph.add(new Paragraph());
        paragraph.add("Rechnungsnummer: " + rechnung.getRechnungsNummer());
        paragraph.setAlignment(Element.ALIGN_LEFT);
        paragraph.setIndentationLeft(30);

        PdfPTable table = new PdfPTable(3);
        table.setHeaderRows(1);
        table.setWidthPercentage(85);
        table.setWidths(new float[]{3.0f, 1.0f, 1.0f});

        PdfPCell column1 = new PdfPCell(new Phrase("Artikel"));
        column1.setHorizontalAlignment(Element.ALIGN_CENTER);
        table.addCell(column1);

        PdfPCell column2 = new PdfPCell(new Phrase("MwSt."));
        column2.setHorizontalAlignment(Element.ALIGN_CENTER);
        table.addCell(column2);

        PdfPCell column3 = new PdfPCell(new Phrase("Preis"));
        column3.setHorizontalAlignment(Element.ALIGN_CENTER);
        table.addCell(column3);


        table.addCell(rechnung.getBestellung().getArtikel().getArtikelBezeichnung());
        table.addCell(rechnung.getBestellung().getArtikel().getMehrwertSteuer() + " %");
        Double test1 = round(rechnung.getBestellung().getArtikel().getPreis(), 2);
        table.addCell(test1.toString());

        document.add(paragraph);
        printEmptyLine();
        document.add(table);
    }

    //    Textteil
    private void printText() {
        Paragraph paragraph = new Paragraph();
        paragraph.add("Vielen Dank für Ihre Bestellung.");
        paragraph.setAlignment(Element.ALIGN_LEFT);
        paragraph.setIndentationLeft(30);

        document.add(paragraph);
    }

//    Signatur


//    Fußzeile


    //    Runden
    private double round(double value, int decimalPoints) {
        double d = Math.pow(10, decimalPoints);
        return Math.round(value * d) / d;
    }


}

