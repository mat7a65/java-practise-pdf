public class Absender {

    private String unternehmen;
    private String strasse;
    private String plz;
    private String stadt;

    public Absender(String unternehmen, String strasse, String plz, String stadt) {
        this.unternehmen = unternehmen;
        this.strasse = strasse;
        this.plz = plz;
        this.stadt = stadt;
    }

    public String getUnternehmen() {
        return unternehmen;
    }

    public void setUnternehmen(String unternehmen) {
        this.unternehmen = unternehmen;
    }

    public String getStrasse() {
        return strasse;
    }

    public void setStrasse(String strasse) {
        this.strasse = strasse;
    }

    public String getPlz() {
        return plz;
    }

    public void setPlz(String plz) {
        this.plz = plz;
    }

    public String getStadt() {
        return stadt;
    }

    public void setStadt(String stadt) {
        this.stadt = stadt;
    }
}
