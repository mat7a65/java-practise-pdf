import java.util.List;

public class Bestellung {

    private int bestellNummer;
    private String bestellDatum;
    private String lieferDatum;
    private Kunde kunde;
    private Artikel artikel;

    public Bestellung(int bestellNummer, String bestellDatum,
                      String lieferDatum, Kunde kunde, Artikel artikel) {
        this.bestellNummer = bestellNummer;
        this.bestellDatum = bestellDatum;
        this.lieferDatum = lieferDatum;
        this.kunde = kunde;
        this.artikel = artikel;

    }

    public int getBestellNummer() {
        return bestellNummer;
    }

    public void setBestellNummer(int bestellNummer) {
        this.bestellNummer = bestellNummer;
    }

    public String getBestellDatum() {
        return bestellDatum;
    }

    public void setBestellDatum(String bestellDatum) {
        this.bestellDatum = bestellDatum;
    }

    public String getLieferDatum() {
        return lieferDatum;
    }

    public void setLieferDatum(String lieferDatum) {
        this.lieferDatum = lieferDatum;
    }

    public Kunde getKunde() {
        return kunde;
    }

    public void setKunde(Kunde kunde) {
        this.kunde = kunde;
    }

    public Artikel getArtikel() {
        return artikel;
    }

    public void setArtikel(Artikel artikel) {
        this.artikel = artikel;
    }
}
