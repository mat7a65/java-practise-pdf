package de.awacademy.javapdf;

import com.lowagie.text.Document;
import com.lowagie.text.PageSize;
import com.lowagie.text.Paragraph;
import com.lowagie.text.pdf.PdfWriter;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;

public class A01_SimplePDFDocument_Aufgabe1 {
    public static void main(String[] args) throws FileNotFoundException {
        new A01_SimplePDFDocument_Aufgabe1().run();
    }

    private void run() throws FileNotFoundException {
        File pdfFile = new File("simpleDocument.pdf");
        Document document = new Document(PageSize.A4);
        PdfWriter.getInstance(document, new FileOutputStream(pdfFile));
        document.open();
        document.add(new Paragraph("Herzlich Willkommen zum Workshop Java - PDF!"));
        document.close();
    }

    // Aufgabe 1: Programm zum Laufen bringen und dieses PDF-Dokument erzeugen.
}
